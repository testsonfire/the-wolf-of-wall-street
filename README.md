# Как развернуть у себя #

1) Установить Meteor [отсюда](https://www.meteor.com/install)

2) Склонировать репозиторий

```
#!bash
$ git clone git@bitbucket.org:ngseos/the-wolf-of-wall-street.git
```
или (если не настроен SSH)
```
#!bash
$ git clone https://yuriy-os@bitbucket.org/ngseos/the-wolf-of-wall-street.git
```
3) Зайти в папку с проектом

4) Сначала установить npm зависимости
```
#!bash
$ meteor npm install
```

5) Установить meteor зависимости
```
#!bash
$ meteor update
```

6) Запустить приложение
```
#!bash
$ meteor
```

# Как добавлять изменения #

Issue-трекер проекта в [Trello](https://trello.com/b/vl4KXmjn/the-wolf-of-poas-project).

Вся разработка ведется в ветке **dev**. На трекере каждое issue имеет код, поэтому, при работе над определенной задачей, под неё создается отдельная от **dev** ветка с номером этого issue, например, ISSUE-102.

# Структура проекта (актуальна на 23.11.2016) #

Подробнее о структуре Meteor приложений можно узнать [здесь](https://guide.meteor.com/structure.html)

```
#!bash

├── client                                              # директория с кодом, который выполняется на стороне клиента 
│   ├── head.html                                       # хедер, который будет на каждой странице приложения
│   └── main.js                                         # импорты выполняемого на стороне клиента кода
│
├── imports                                             # директория, в которой содержится основной код приложения
│   ├── api                                             # директория, в которой содержится основная логика приложения
│   │   ├── role-assign                                 # директория с логикой механизма назначения ролей пользователям
│   │   │   └── methods
│   │   │       └── methods.js                          # методы, которые отвечают за назначение ролей (Волк, Коп, Банк) пользователям 
│   │   ├── shares                                      # директория с логикой описывающей Акцию
│   │   │   ├── methods
│   │   │   │   └── methods.js                          # методы для работы с Акциями (подробнее о методах https://guide.meteor.com/methods.html)
│   │   │   ├── server
│   │   │   │   └── publications.js                     # публикации связанные с Акциями (подробнее о публикациях https://guide.meteor.com/data-loading.html)
│   │   │   └── shares.js                               # схема для Акции для коллекции объектов в БД (подробнее https://guide.meteor.com/collections.html)
│   │   └── wolf                                        # аналогичная структура директории для Волка
│   │       ├── methods
│   │       │   └── methods.js
│   │       ├── server
│   │       │   └── publications.js
│   │       └── wolf.js
│   ├── startup                                         # директория с настройками приложения (импортируется и компилируется первой)
│   │   ├── both
│   │   │   ├── index.js
│   │   │   └── useraccounts-config.js                  # настройки аутентификации 
│   │   ├── client
│   │   │   ├── index.js
│   │   │   └── routes.js                               # роутер
│   │   └── server
│   │       ├── index.js
│   │       └── register-api.js                         # регистрация всех методов приложения, чтобы их видел и клиент и сервер 
│   └── ui                                              # директория, которая содержит файлы, связанные пользовательским интерфейсом
│       ├── layouts                                     # директория содержит либо лейауты страниц, либо одноразово используемые страницы
│       │   ├── dashboard                               # каркас для главных страниц всех пользователей
│       │   │   ├── dashboard.css
│       │   │   ├── dashboard.html
│       │   │   └── dashboard.js
│       │   ├── login-page                              # страница логина
│       │   │   ├── login-page.html
│       │   │   └── login-page.js
│       │   └── role-pick                               # страница выбора роли
│       │       ├── role-pick.css
│       │       ├── role-pick.html
│       │       └── role-pick.js
│       └── pages                                       # директория с отдельными для каждого пользователя интерфейсами
│           ├── cop
│           │   ├── bank
│           │   │   ├── bank.html
│           │   │   └── bank.js
│           │   └── work
│           │       ├── work.html
│           │       └── work.js
│           └── wolf
│               ├── bank
│               │   ├── bank.html
│               │   └── bank.js
│               ├── market
│               │   ├── market.html
│               │   └── market.js
│               └── portfolio
│                   ├── portfolio.html
│                   └── portfolio.js
├── node_modules
├── package.json
├── README.md
└── server
    └── main.js

```