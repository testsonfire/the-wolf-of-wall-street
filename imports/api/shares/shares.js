import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';


export const Shares = new Mongo.Collection('Shares');

Shares.schema = new SimpleSchema({
    _id: { type: String, regEx: SimpleSchema.RegEx.Id },
    name: { type: String },
    price: { type: Number, defaultValue: 0 },
    countForSale: { type: Number, defaultValue: 0 },
    countOwned: { type: Number, defaultValue: 0 },
    percent: { type: Number, defaultValue: 0 },
    status: { type: String, regEx: /^(up|down)$/ },
    userId: { type: String, regEx: SimpleSchema.RegEx.Id, optional: true },
    checked: { type: Boolean, defaultValue: false}
});

Shares.attachSchema(Shares.schema);