import { Meteor } from 'meteor/meteor';
import { Shares } from '../shares.js';

Meteor.publish('shares', function sharesPublication() {
    return Shares.find({});
});