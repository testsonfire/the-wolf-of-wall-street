import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Wolf } from '../wolf.js';

export const wolf_insert = new ValidatedMethod({

    name: 'wolf.insert',

    validate: new SimpleSchema({
        userId: {
            type: String,
            regEx: SimpleSchema.RegEx.Id
        },
    }).validator(),

    run({ userId }) {
        return Wolf.insert({ userId: userId });
    },
});