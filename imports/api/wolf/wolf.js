import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const Wolf = new Mongo.Collection('Wolf');

Wolf.schema = new SimpleSchema({
    _id: { type: String, regEx: SimpleSchema.RegEx.Id },
    userId: { type: String, regEx: SimpleSchema.RegEx.Id},
    rating: { type: Number, defaultValue: 75 },
    money: { type: Number, defaultValue: 40000 },
    shares: {type: [String], optional: true},
    credits: {type: [String], optional: true}
});

Wolf.attachSchema(Wolf.schema);