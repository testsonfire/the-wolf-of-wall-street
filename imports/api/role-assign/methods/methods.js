import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Roles } from 'meteor/alanning:roles'

Meteor.methods({
  assign_role: function (role) {
    check(role, [String]);

    Roles.addUsersToRoles(this.userId, role);
  }
});