import { Template } from 'meteor/templating';
import { AccountsTemplates } from 'meteor/useraccounts:core'

import './dashboard.css';
import './dashboard.html';

Template.dashboard_layout.events({
    'click .logout': function (event) {
        event.preventDefault();
        AccountsTemplates.logout();
    }
});