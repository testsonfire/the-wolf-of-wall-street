import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { wolf_insert } from '../../../api/wolf/methods/methods.js';

import './role-pick.html';
import './role-pick.css';


Template.role_pick_layout.events({
    'click .wolf': function(event) {
        Meteor.call('assign_role', ["wolf"], (err, res) => {
            if (err) {
                throw new Meteor.Error('err');
            }
        });

        let userId = Meteor.userId();

        wolf_insert.call({ userId }, (err) => {
            if (err) {
            }
        });
    },
    'click .cop': function(event) {
        Meteor.call('assign_role', ["cop"], (err, res) => {
            if (err) {
                throw new Meteor.Error('err');
            }
        });
    }

});