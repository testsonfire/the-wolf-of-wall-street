import { Template } from 'meteor/templating';
import { Shares } from '../../../../api/shares/shares.js';

import './market.html';

Template.wolf_market.onCreated(function marketOnCreated() {
    this.subscribe('shares');
});

Template.wolf_market.helpers({
    shares() {
        return Shares.find({});
    }
});
