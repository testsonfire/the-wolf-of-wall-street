import { Router } from 'meteor/iron:router'

import '../../ui/layouts/dashboard/dashboard.js';
import '../../ui/layouts/login-page/login-page.js';
import '../../ui/layouts/role-pick/role-pick.js';

import '../../ui/pages/wolf/portfolio/portfolio.js';
import '../../ui/pages/wolf/bank/bank.js';
import '../../ui/pages/wolf/market/market.js';

import '../../ui/pages/cop/work/work.js';
import '../../ui/pages/cop/bank/bank.js';


Router.route('/role', function() {
    this.layout('role_pick_layout');
}, {
        name: 'role'
    });

Router.route('/', function() {
    this.layout('login_page_layout');
}, {
        name: 'login'
    });

Router.route('/wolf', function() {

    this.layout('dashboard_layout');

    this.render('wolf_brand', { to: 'brand' });

    this.render('wolf_aside', { to: 'aside' });

    this.render('wolf_portfolio', { to: 'content' });
}, {
        name: 'wolf'
    });

Router.route('/wolf/portfolio', function() {
    this.layout('dashboard_layout');
    this.redirect('/wolf');
}, {
        name: 'wolf.portfolio'
    });

Router.route('/wolf/market', function() {

    this.layout('dashboard_layout');

    this.render('wolf_brand', { to: 'brand' });

    this.render('wolf_aside', { to: 'aside' });

    this.render('wolf_market', { to: 'content' });
}, {
        name: 'wolf.market'
    });


Router.route('/wolf/bank', function() {

    this.layout('dashboard_layout');

    this.render('wolf_brand', { to: 'brand' });

    this.render('wolf_aside', { to: 'aside' });

    this.render('wolf_bank', { to: 'content' });
}, {
        name: 'wolf.bank'
    });



Router.route('/cop', function() {

    this.layout('dashboard_layout');

    this.render('cop_brand', { to: 'brand' });

    this.render('cop_aside', { to: 'aside' });

    this.render('cop_work', { to: 'content' });
}, {
        name: 'cop'
    });

Router.route('/cop/work', function() {

    this.layout('dashboard_layout');
    this.redirect('/cop');
}, {
        name: 'cop.work'
    });

Router.route('/cop/bank', function() {

    this.layout('dashboard_layout');

    this.render('cop_brand', { to: 'brand' });

    this.render('cop_aside', { to: 'aside' });

    this.render('cop_bank', { to: 'content' });
}, {
        name: 'cop.bank'
    });