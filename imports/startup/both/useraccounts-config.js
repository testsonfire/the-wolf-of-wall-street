import { Router } from 'meteor/iron:router'
import { AccountsTemplates } from 'meteor/useraccounts:core'

let submitFunc = function(error, state){
  if (!error) {
    if (state === "signUp") {
        Router.go('/role');
    }

    if (state === "signIn") {
        let userRole = Roles.getRolesForUser( Meteor.userId())[0];
        
        if(userRole === 'undefined') {
          Router.go('/role');
        } else {
          Router.go('/' + userRole);
        }
    }
  }
};

let logoutFunc = function(error, state){
  if (!error) {
    Router.go('/');
  }
};

AccountsTemplates.configure({
    // Behavior
    confirmPassword: true,
    enablePasswordChange: true,
    forbidClientAccountCreation: false,
    overrideLoginErrors: true,
    sendVerificationEmail: false,
    lowercaseUsername: false,
    focusFirstInput: true,

    // Appearance
    showAddRemoveServices: false,
    showForgotPasswordLink: false,
    showLabels: true,
    showPlaceholders: true,
    showResendVerificationEmailLink: false,

    // Client-side Validation
    continuousValidation: false,
    negativeFeedback: false,
    negativeValidation: true,
    positiveValidation: true,
    positiveFeedback: true,
    showValidating: true,
    
    // Hooks
    onSubmitHook: submitFunc,
    onLogoutHook: logoutFunc,

});

